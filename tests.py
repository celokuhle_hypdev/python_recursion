import unittest
import replace as app


class ReplaceFunctionTest(unittest.TestCase):

    def setUp(self):
        unittest.TestCase.setUp(self)
            
    def test_no_replace_if_searchstring_is_longerthan_originalstring(self):
        original = "shorter"
        self.assertEquals(original, app.replace(original, "verylong", "@@"))
        
    def test_replace_passes_if_searchstring_is_shorterthan_replacestring(self):
        original = "Hello world"
        expected = "He<<000 world"
        self.assertEquals(expected, app.replace(original, "llo", "<<000"))

    def test_replace_passes_if_searchstring_is_longerthan_replacestring(self):
        original = "Hello world"
        expected = "He@@ world"
        self.assertEquals(expected, app.replace(original, "llo", "@@"))
