'''
Created on 16 Feb 2021

@author: celokuhle.myeza
'''

def replace(original, search, replaceWith):
    length = len(original)
    searchLength = len(search)
          
    if(searchLength > length):
        return original;
    
    batch = original[:searchLength]  # substring similar in size to the search string
    
    if(batch == search):
        remainder = original[searchLength:]
        batch = replaceWith
    else:
        remainder = original[1:]  # substring excluding one character on the left
        batch = batch[0]
    
    return batch + replace(remainder, search, replaceWith);

#manual tests, there is a unit test file as well
if __name__ == "__main__":  #prevents this part from executing whenever this module is imported elsewhere
    print(replace("Hello world", "llo", "@@"))
    print(replace("Hello world", "Hello", "@@"))
    print(replace("Hello world", "o w", "+++"))
    print(replace("Hello world", "world", "umhlaba"))
    
